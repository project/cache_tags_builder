## Cache Tags Builder
Provide service which build core supported cache tags.
Those tags are user by core in different places, and some times it is hard to find.

Cache tags which we build:
- `ENTITY_TYPE:ENTITY_ID`(node:1)
- `ENTITY_TYPE_list`(node_list)
- `ENTITY_TYPE_list:ENTITY_BUNDLE`(node_list:news)

You can use these tags for your custom code, it will be flushed by the core when entity is updated or deleted.
