<?php

namespace Drupal\cache_tags_builder\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class CacheTagsBuilder.
 */
class CacheTagsBuilder {

  /**
   * Get entity individual tags.
   *
   * @see \Drupal\Core\Entity\EntityBase::getCacheTags()
   *
   * @return string[]
   *   Cache tags.
   */
  public function getEntityIndividualTags(EntityInterface $entity) {
    return $entity->getCacheTags();
  }

  /**
   * Get entity type cache tags.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @see \Drupal\Core\Entity\EntityType
   *
   * @return string[]
   *   Cache tags.
   */
  public function getEntityTypeListTags(EntityInterface $entity) {
    return $entity->getEntityType()->getListCacheTags();
  }

  /**
   * Get entity bundle related list cache tags.
   *
   * Because the getListCacheTagsToInvalidate is protected and we can't use it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @see \Drupal\Core\Entity\EntityBase::getListCacheTagsToInvalidate()
   *
   * @return string[]
   *   Cache tags.
   */
  public function getEntityBundleListTags(EntityInterface $entity) {
    $prefix = $entity->getEntityTypeId() . '_list';
    return Cache::buildTags($prefix, [$entity->bundle()]);
  }

}
