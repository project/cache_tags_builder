<?php

namespace Drupal\Tests\cache_tags_builder\Unit;

use Drupal\cache_tags_builder\Service\CacheTagsBuilder;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Tests\UnitTestCase;

/**
 * Class CacheTagBuilderTest.
 *
 * @package Drupal\Tests\cache_tags_builder\Unit
 *
 * @group cache_tags_builder
 */
class CacheTagBuilderTest extends UnitTestCase {

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $container = new ContainerBuilder();
    $builder = new CacheTagsBuilder();
    $container->set('cache_tags_builder.manager', $builder);
    \Drupal::setContainer($container);
  }

  /**
   * Checks if the service is created in the Drupal context.
   */
  public function testTagsBuilder() {
    $this->assertNotNull(\Drupal::service('cache_tags_builder.manager'));
  }

  /**
   * Test entity list cache tag build.
   */
  public function testEntityListTabBuild() {
    $entity_type = new ContentEntityType([
      'id' => 'test_entity_type',
      'bundle_entity_type' => 'entity_test_bundle',
    ]);
    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->getEntityTypeId()->willReturn($entity_type->id());
    $entity->bundle()->willReturn('test_bundle');
    $keys = \Drupal::service('cache_tags_builder.manager')->getEntityBundleListTags($entity->reveal());
    $this->assertEquals(['test_entity_type_list:test_bundle'], $keys);
  }

}
